package com.chi.wsclient.osgi;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import com.chi.wsclient.stubs.*;

@Component

@Service
public class WaitTimeServiceImpl implements WaitTimeService {

	@Override
	public String getWaitTimes() {
		String waitTime = "<No Wait time found>";

	    // log("Inside main 2.8");
	    initTracing();
	    IUtility2014 utility2014 = new Utility().getIUtility2014();

	    javax.xml.ws.BindingProvider bp = (javax.xml.ws.BindingProvider) utility2014;
	    javax.xml.ws.soap.SOAPBinding binding = (javax.xml.ws.soap.SOAPBinding) bp.getBinding();
	    binding.setMTOMEnabled(false);

	    UtilityGetDepartmentWaitTimesResponse departmentWaitTimes = null;
		try {
			departmentWaitTimes = utility2014.getDepartmentWaitTimes(60, true, true, true, createDepartments(), null, null);
		} catch (IUtility2014GetDepartmentWaitTimesInternalFaultFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IUtility2014GetDepartmentWaitTimesApplicationFaultFaultMessage e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // log("After System.call, departmentWaitTimes=[" + departmentWaitTimes + "]");
	    
	    waitTime = String.format("%s ==> %s, %s == %s", 
								"TotalPatientsForAverage", 
								departmentWaitTimes.getTotalPatientsForAverage(),
								"LongestCurrentWaitTime", 
								departmentWaitTimes.getLongestCurrentWaitTime());
	    
	    return waitTime;
	  }

	  private static void initTracing() {
	    System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
	    System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
	    System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
	    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

	  }

	  private static ArrayOfIDType createDepartments() {
	    final ObjectFactory objectFactory = new ObjectFactory();
	    final ArrayOfIDType departments = new ArrayOfIDType();
	    final IDType department = new IDType();
	    department.setID(objectFactory.createIDTypeID("101031100"));
	    departments.getIDType().add(department);
	    return departments;
	  }
}
