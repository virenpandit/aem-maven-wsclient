
package com.chi.wsclient.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Utility.GetDepartmentWaitTimesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Utility.GetDepartmentWaitTimesResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AverageWaitTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DepartmentWaitTimes" type="{urn:Epic-com:Scheduling.2014.Services.Utility}ArrayOfDepartmentWaitTimes" minOccurs="0"/&gt;
 *         &lt;element name="LongestCurrentWaitTime" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="PatientsStillWaiting" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="TotalPatientsForAverage" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Utility.GetDepartmentWaitTimesResponse", propOrder = {
    "averageWaitTime",
    "departmentWaitTimes",
    "longestCurrentWaitTime",
    "patientsStillWaiting",
    "totalPatientsForAverage"
})
public class UtilityGetDepartmentWaitTimesResponse {

    @XmlElementRef(name = "AverageWaitTime", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<String> averageWaitTime;
    @XmlElementRef(name = "DepartmentWaitTimes", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfDepartmentWaitTimes> departmentWaitTimes;
    @XmlElement(name = "LongestCurrentWaitTime")
    protected Integer longestCurrentWaitTime;
    @XmlElement(name = "PatientsStillWaiting")
    protected Integer patientsStillWaiting;
    @XmlElement(name = "TotalPatientsForAverage")
    protected Integer totalPatientsForAverage;

    /**
     * Gets the value of the averageWaitTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAverageWaitTime() {
        return averageWaitTime;
    }

    /**
     * Sets the value of the averageWaitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAverageWaitTime(JAXBElement<String> value) {
        this.averageWaitTime = value;
    }

    /**
     * Gets the value of the departmentWaitTimes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfDepartmentWaitTimes }{@code >}
     *     
     */
    public JAXBElement<ArrayOfDepartmentWaitTimes> getDepartmentWaitTimes() {
        return departmentWaitTimes;
    }

    /**
     * Sets the value of the departmentWaitTimes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfDepartmentWaitTimes }{@code >}
     *     
     */
    public void setDepartmentWaitTimes(JAXBElement<ArrayOfDepartmentWaitTimes> value) {
        this.departmentWaitTimes = value;
    }

    /**
     * Gets the value of the longestCurrentWaitTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLongestCurrentWaitTime() {
        return longestCurrentWaitTime;
    }

    /**
     * Sets the value of the longestCurrentWaitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLongestCurrentWaitTime(Integer value) {
        this.longestCurrentWaitTime = value;
    }

    /**
     * Gets the value of the patientsStillWaiting property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPatientsStillWaiting() {
        return patientsStillWaiting;
    }

    /**
     * Sets the value of the patientsStillWaiting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPatientsStillWaiting(Integer value) {
        this.patientsStillWaiting = value;
    }

    /**
     * Gets the value of the totalPatientsForAverage property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalPatientsForAverage() {
        return totalPatientsForAverage;
    }

    /**
     * Sets the value of the totalPatientsForAverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalPatientsForAverage(Integer value) {
        this.totalPatientsForAverage = value;
    }

}
