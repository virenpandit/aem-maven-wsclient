
package com.chi.wsclient.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header_vals" type="{urn:Epic-com:Scheduling.2014.Services.Utility}GetDepartmentWaitTimesRequest" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVals"
})
@XmlRootElement(name = "GetDepartmentWaitTimes_Json")
public class GetDepartmentWaitTimesJson {

    @XmlElementRef(name = "header_vals", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<GetDepartmentWaitTimesRequest> headerVals;

    /**
     * Gets the value of the headerVals property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetDepartmentWaitTimesRequest }{@code >}
     *     
     */
    public JAXBElement<GetDepartmentWaitTimesRequest> getHeaderVals() {
        return headerVals;
    }

    /**
     * Sets the value of the headerVals property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetDepartmentWaitTimesRequest }{@code >}
     *     
     */
    public void setHeaderVals(JAXBElement<GetDepartmentWaitTimesRequest> value) {
        this.headerVals = value;
    }

}
