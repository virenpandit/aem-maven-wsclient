
package com.chi.wsclient.stubs;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.chi.wsclient.stubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UtilityGetDepartmentWaitTimesResponse_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "Utility.GetDepartmentWaitTimesResponse");
    private final static QName _ArrayOfDepartmentWaitTimes_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "ArrayOfDepartmentWaitTimes");
    private final static QName _DepartmentWaitTimes_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "DepartmentWaitTimes");
    private final static QName _GetDepartmentWaitTimesRequest_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "GetDepartmentWaitTimesRequest");
    private final static QName _ArrayOfIDType_QNAME = new QName("urn:Epic-com:Scheduling.2012.Services.Patient", "ArrayOfIDType");
    private final static QName _IDType_QNAME = new QName("urn:Epic-com:Scheduling.2012.Services.Patient", "IDType");
    private final static QName _FaultType_QNAME = new QName("urn:epicsystems.com:Interconnect.2004-05.Faults", "FaultType");
    private final static QName _InternalFault_QNAME = new QName("urn:epicsystems.com:Interconnect.2004-05.Faults", "InternalFault");
    private final static QName _ApplicationFault_QNAME = new QName("urn:epicsystems.com:Interconnect.2004-05.Faults", "ApplicationFault");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _GetDepartmentWaitTimesMinutesToLookBack_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "MinutesToLookBack");
    private final static QName _GetDepartmentWaitTimesUseAppointmentTimeIfAfterStartEvent_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "UseAppointmentTimeIfAfterStartEvent");
    private final static QName _GetDepartmentWaitTimesCalculateForOverallLevel_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "CalculateForOverallLevel");
    private final static QName _GetDepartmentWaitTimesCalculateForEachDepartment_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "CalculateForEachDepartment");
    private final static QName _GetDepartmentWaitTimesDepartments_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "Departments");
    private final static QName _GetDepartmentWaitTimesStartEvent_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "StartEvent");
    private final static QName _GetDepartmentWaitTimesEndEvent_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "EndEvent");
    private final static QName _GetDepartmentWaitTimesResponseGetDepartmentWaitTimesResult_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "GetDepartmentWaitTimesResult");
    private final static QName _GetDepartmentWaitTimesXmlHeaderVals_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "header_vals");
    private final static QName _GetDepartmentWaitTimesXmlResponseGetDepartmentWaitTimesXmlResult_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "GetDepartmentWaitTimes_XmlResult");
    private final static QName _GetDepartmentWaitTimesJsonResponseGetDepartmentWaitTimesJsonResult_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "GetDepartmentWaitTimes_JsonResult");
    private final static QName _DepartmentWaitTimesAverageWaitTime_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "AverageWaitTime");
    private final static QName _DepartmentWaitTimesDepartmentIDs_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "DepartmentIDs");
    private final static QName _DepartmentWaitTimesDepartmentName_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "DepartmentName");
    private final static QName _DepartmentWaitTimesLongestCurrentWaitTime_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "LongestCurrentWaitTime");
    private final static QName _DepartmentWaitTimesTotalPatientsForAverage_QNAME = new QName("urn:Epic-com:Scheduling.2014.Services.Utility", "TotalPatientsForAverage");
    private final static QName _IDTypeID_QNAME = new QName("urn:Epic-com:Scheduling.2012.Services.Patient", "ID");
    private final static QName _IDTypeType_QNAME = new QName("urn:Epic-com:Scheduling.2012.Services.Patient", "Type");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.chi.wsclient.stubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimes }
     * 
     */
    public GetDepartmentWaitTimes createGetDepartmentWaitTimes() {
        return new GetDepartmentWaitTimes();
    }

    /**
     * Create an instance of {@link ArrayOfIDType }
     * 
     */
    public ArrayOfIDType createArrayOfIDType() {
        return new ArrayOfIDType();
    }

    /**
     * Create an instance of {@link IDType }
     * 
     */
    public IDType createIDType() {
        return new IDType();
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimesResponse }
     * 
     */
    public GetDepartmentWaitTimesResponse createGetDepartmentWaitTimesResponse() {
        return new GetDepartmentWaitTimesResponse();
    }

    /**
     * Create an instance of {@link UtilityGetDepartmentWaitTimesResponse }
     * 
     */
    public UtilityGetDepartmentWaitTimesResponse createUtilityGetDepartmentWaitTimesResponse() {
        return new UtilityGetDepartmentWaitTimesResponse();
    }

    /**
     * Create an instance of {@link ArrayOfDepartmentWaitTimes }
     * 
     */
    public ArrayOfDepartmentWaitTimes createArrayOfDepartmentWaitTimes() {
        return new ArrayOfDepartmentWaitTimes();
    }

    /**
     * Create an instance of {@link DepartmentWaitTimes }
     * 
     */
    public DepartmentWaitTimes createDepartmentWaitTimes() {
        return new DepartmentWaitTimes();
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimesXml }
     * 
     */
    public GetDepartmentWaitTimesXml createGetDepartmentWaitTimesXml() {
        return new GetDepartmentWaitTimesXml();
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimesRequest }
     * 
     */
    public GetDepartmentWaitTimesRequest createGetDepartmentWaitTimesRequest() {
        return new GetDepartmentWaitTimesRequest();
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimesXmlResponse }
     * 
     */
    public GetDepartmentWaitTimesXmlResponse createGetDepartmentWaitTimesXmlResponse() {
        return new GetDepartmentWaitTimesXmlResponse();
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimesJson }
     * 
     */
    public GetDepartmentWaitTimesJson createGetDepartmentWaitTimesJson() {
        return new GetDepartmentWaitTimesJson();
    }

    /**
     * Create an instance of {@link GetDepartmentWaitTimesJsonResponse }
     * 
     */
    public GetDepartmentWaitTimesJsonResponse createGetDepartmentWaitTimesJsonResponse() {
        return new GetDepartmentWaitTimesJsonResponse();
    }

    /**
     * Create an instance of {@link FaultType }
     * 
     */
    public FaultType createFaultType() {
        return new FaultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UtilityGetDepartmentWaitTimesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "Utility.GetDepartmentWaitTimesResponse")
    public JAXBElement<UtilityGetDepartmentWaitTimesResponse> createUtilityGetDepartmentWaitTimesResponse(UtilityGetDepartmentWaitTimesResponse value) {
        return new JAXBElement<UtilityGetDepartmentWaitTimesResponse>(_UtilityGetDepartmentWaitTimesResponse_QNAME, UtilityGetDepartmentWaitTimesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDepartmentWaitTimes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "ArrayOfDepartmentWaitTimes")
    public JAXBElement<ArrayOfDepartmentWaitTimes> createArrayOfDepartmentWaitTimes(ArrayOfDepartmentWaitTimes value) {
        return new JAXBElement<ArrayOfDepartmentWaitTimes>(_ArrayOfDepartmentWaitTimes_QNAME, ArrayOfDepartmentWaitTimes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepartmentWaitTimes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "DepartmentWaitTimes")
    public JAXBElement<DepartmentWaitTimes> createDepartmentWaitTimes(DepartmentWaitTimes value) {
        return new JAXBElement<DepartmentWaitTimes>(_DepartmentWaitTimes_QNAME, DepartmentWaitTimes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepartmentWaitTimesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "GetDepartmentWaitTimesRequest")
    public JAXBElement<GetDepartmentWaitTimesRequest> createGetDepartmentWaitTimesRequest(GetDepartmentWaitTimesRequest value) {
        return new JAXBElement<GetDepartmentWaitTimesRequest>(_GetDepartmentWaitTimesRequest_QNAME, GetDepartmentWaitTimesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2012.Services.Patient", name = "ArrayOfIDType")
    public JAXBElement<ArrayOfIDType> createArrayOfIDType(ArrayOfIDType value) {
        return new JAXBElement<ArrayOfIDType>(_ArrayOfIDType_QNAME, ArrayOfIDType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2012.Services.Patient", name = "IDType")
    public JAXBElement<IDType> createIDType(IDType value) {
        return new JAXBElement<IDType>(_IDType_QNAME, IDType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epicsystems.com:Interconnect.2004-05.Faults", name = "FaultType")
    public JAXBElement<FaultType> createFaultType(FaultType value) {
        return new JAXBElement<FaultType>(_FaultType_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epicsystems.com:Interconnect.2004-05.Faults", name = "InternalFault")
    public JAXBElement<FaultType> createInternalFault(FaultType value) {
        return new JAXBElement<FaultType>(_InternalFault_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:epicsystems.com:Interconnect.2004-05.Faults", name = "ApplicationFault")
    public JAXBElement<FaultType> createApplicationFault(FaultType value) {
        return new JAXBElement<FaultType>(_ApplicationFault_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "MinutesToLookBack", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<Integer> createGetDepartmentWaitTimesMinutesToLookBack(Integer value) {
        return new JAXBElement<Integer>(_GetDepartmentWaitTimesMinutesToLookBack_QNAME, Integer.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "UseAppointmentTimeIfAfterStartEvent", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<Boolean> createGetDepartmentWaitTimesUseAppointmentTimeIfAfterStartEvent(Boolean value) {
        return new JAXBElement<Boolean>(_GetDepartmentWaitTimesUseAppointmentTimeIfAfterStartEvent_QNAME, Boolean.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "CalculateForOverallLevel", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<Boolean> createGetDepartmentWaitTimesCalculateForOverallLevel(Boolean value) {
        return new JAXBElement<Boolean>(_GetDepartmentWaitTimesCalculateForOverallLevel_QNAME, Boolean.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "CalculateForEachDepartment", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<Boolean> createGetDepartmentWaitTimesCalculateForEachDepartment(Boolean value) {
        return new JAXBElement<Boolean>(_GetDepartmentWaitTimesCalculateForEachDepartment_QNAME, Boolean.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "Departments", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<ArrayOfIDType> createGetDepartmentWaitTimesDepartments(ArrayOfIDType value) {
        return new JAXBElement<ArrayOfIDType>(_GetDepartmentWaitTimesDepartments_QNAME, ArrayOfIDType.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "StartEvent", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<IDType> createGetDepartmentWaitTimesStartEvent(IDType value) {
        return new JAXBElement<IDType>(_GetDepartmentWaitTimesStartEvent_QNAME, IDType.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "EndEvent", scope = GetDepartmentWaitTimes.class)
    public JAXBElement<IDType> createGetDepartmentWaitTimesEndEvent(IDType value) {
        return new JAXBElement<IDType>(_GetDepartmentWaitTimesEndEvent_QNAME, IDType.class, GetDepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UtilityGetDepartmentWaitTimesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "GetDepartmentWaitTimesResult", scope = GetDepartmentWaitTimesResponse.class)
    public JAXBElement<UtilityGetDepartmentWaitTimesResponse> createGetDepartmentWaitTimesResponseGetDepartmentWaitTimesResult(UtilityGetDepartmentWaitTimesResponse value) {
        return new JAXBElement<UtilityGetDepartmentWaitTimesResponse>(_GetDepartmentWaitTimesResponseGetDepartmentWaitTimesResult_QNAME, UtilityGetDepartmentWaitTimesResponse.class, GetDepartmentWaitTimesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepartmentWaitTimesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "header_vals", scope = GetDepartmentWaitTimesXml.class)
    public JAXBElement<GetDepartmentWaitTimesRequest> createGetDepartmentWaitTimesXmlHeaderVals(GetDepartmentWaitTimesRequest value) {
        return new JAXBElement<GetDepartmentWaitTimesRequest>(_GetDepartmentWaitTimesXmlHeaderVals_QNAME, GetDepartmentWaitTimesRequest.class, GetDepartmentWaitTimesXml.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UtilityGetDepartmentWaitTimesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "GetDepartmentWaitTimes_XmlResult", scope = GetDepartmentWaitTimesXmlResponse.class)
    public JAXBElement<UtilityGetDepartmentWaitTimesResponse> createGetDepartmentWaitTimesXmlResponseGetDepartmentWaitTimesXmlResult(UtilityGetDepartmentWaitTimesResponse value) {
        return new JAXBElement<UtilityGetDepartmentWaitTimesResponse>(_GetDepartmentWaitTimesXmlResponseGetDepartmentWaitTimesXmlResult_QNAME, UtilityGetDepartmentWaitTimesResponse.class, GetDepartmentWaitTimesXmlResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepartmentWaitTimesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "header_vals", scope = GetDepartmentWaitTimesJson.class)
    public JAXBElement<GetDepartmentWaitTimesRequest> createGetDepartmentWaitTimesJsonHeaderVals(GetDepartmentWaitTimesRequest value) {
        return new JAXBElement<GetDepartmentWaitTimesRequest>(_GetDepartmentWaitTimesXmlHeaderVals_QNAME, GetDepartmentWaitTimesRequest.class, GetDepartmentWaitTimesJson.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UtilityGetDepartmentWaitTimesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "GetDepartmentWaitTimes_JsonResult", scope = GetDepartmentWaitTimesJsonResponse.class)
    public JAXBElement<UtilityGetDepartmentWaitTimesResponse> createGetDepartmentWaitTimesJsonResponseGetDepartmentWaitTimesJsonResult(UtilityGetDepartmentWaitTimesResponse value) {
        return new JAXBElement<UtilityGetDepartmentWaitTimesResponse>(_GetDepartmentWaitTimesJsonResponseGetDepartmentWaitTimesJsonResult_QNAME, UtilityGetDepartmentWaitTimesResponse.class, GetDepartmentWaitTimesJsonResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "CalculateForEachDepartment", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<Boolean> createGetDepartmentWaitTimesRequestCalculateForEachDepartment(Boolean value) {
        return new JAXBElement<Boolean>(_GetDepartmentWaitTimesCalculateForEachDepartment_QNAME, Boolean.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "CalculateForOverallLevel", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<Boolean> createGetDepartmentWaitTimesRequestCalculateForOverallLevel(Boolean value) {
        return new JAXBElement<Boolean>(_GetDepartmentWaitTimesCalculateForOverallLevel_QNAME, Boolean.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "Departments", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<ArrayOfIDType> createGetDepartmentWaitTimesRequestDepartments(ArrayOfIDType value) {
        return new JAXBElement<ArrayOfIDType>(_GetDepartmentWaitTimesDepartments_QNAME, ArrayOfIDType.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "EndEvent", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<IDType> createGetDepartmentWaitTimesRequestEndEvent(IDType value) {
        return new JAXBElement<IDType>(_GetDepartmentWaitTimesEndEvent_QNAME, IDType.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "MinutesToLookBack", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<Integer> createGetDepartmentWaitTimesRequestMinutesToLookBack(Integer value) {
        return new JAXBElement<Integer>(_GetDepartmentWaitTimesMinutesToLookBack_QNAME, Integer.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "StartEvent", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<IDType> createGetDepartmentWaitTimesRequestStartEvent(IDType value) {
        return new JAXBElement<IDType>(_GetDepartmentWaitTimesStartEvent_QNAME, IDType.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "UseAppointmentTimeIfAfterStartEvent", scope = GetDepartmentWaitTimesRequest.class)
    public JAXBElement<Boolean> createGetDepartmentWaitTimesRequestUseAppointmentTimeIfAfterStartEvent(Boolean value) {
        return new JAXBElement<Boolean>(_GetDepartmentWaitTimesUseAppointmentTimeIfAfterStartEvent_QNAME, Boolean.class, GetDepartmentWaitTimesRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "AverageWaitTime", scope = DepartmentWaitTimes.class)
    public JAXBElement<String> createDepartmentWaitTimesAverageWaitTime(String value) {
        return new JAXBElement<String>(_DepartmentWaitTimesAverageWaitTime_QNAME, String.class, DepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "DepartmentIDs", scope = DepartmentWaitTimes.class)
    public JAXBElement<ArrayOfIDType> createDepartmentWaitTimesDepartmentIDs(ArrayOfIDType value) {
        return new JAXBElement<ArrayOfIDType>(_DepartmentWaitTimesDepartmentIDs_QNAME, ArrayOfIDType.class, DepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "DepartmentName", scope = DepartmentWaitTimes.class)
    public JAXBElement<String> createDepartmentWaitTimesDepartmentName(String value) {
        return new JAXBElement<String>(_DepartmentWaitTimesDepartmentName_QNAME, String.class, DepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "LongestCurrentWaitTime", scope = DepartmentWaitTimes.class)
    public JAXBElement<String> createDepartmentWaitTimesLongestCurrentWaitTime(String value) {
        return new JAXBElement<String>(_DepartmentWaitTimesLongestCurrentWaitTime_QNAME, String.class, DepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "TotalPatientsForAverage", scope = DepartmentWaitTimes.class)
    public JAXBElement<String> createDepartmentWaitTimesTotalPatientsForAverage(String value) {
        return new JAXBElement<String>(_DepartmentWaitTimesTotalPatientsForAverage_QNAME, String.class, DepartmentWaitTimes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "AverageWaitTime", scope = UtilityGetDepartmentWaitTimesResponse.class)
    public JAXBElement<String> createUtilityGetDepartmentWaitTimesResponseAverageWaitTime(String value) {
        return new JAXBElement<String>(_DepartmentWaitTimesAverageWaitTime_QNAME, String.class, UtilityGetDepartmentWaitTimesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfDepartmentWaitTimes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", name = "DepartmentWaitTimes", scope = UtilityGetDepartmentWaitTimesResponse.class)
    public JAXBElement<ArrayOfDepartmentWaitTimes> createUtilityGetDepartmentWaitTimesResponseDepartmentWaitTimes(ArrayOfDepartmentWaitTimes value) {
        return new JAXBElement<ArrayOfDepartmentWaitTimes>(_DepartmentWaitTimes_QNAME, ArrayOfDepartmentWaitTimes.class, UtilityGetDepartmentWaitTimesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2012.Services.Patient", name = "ID", scope = IDType.class)
    public JAXBElement<String> createIDTypeID(String value) {
        return new JAXBElement<String>(_IDTypeID_QNAME, String.class, IDType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:Epic-com:Scheduling.2012.Services.Patient", name = "Type", scope = IDType.class)
    public JAXBElement<String> createIDTypeType(String value) {
        return new JAXBElement<String>(_IDTypeType_QNAME, String.class, IDType.class, value);
    }

}
