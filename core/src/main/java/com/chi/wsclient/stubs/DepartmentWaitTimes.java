
package com.chi.wsclient.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DepartmentWaitTimes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DepartmentWaitTimes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AverageWaitTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DepartmentIDs" type="{urn:Epic-com:Scheduling.2012.Services.Patient}ArrayOfIDType" minOccurs="0"/&gt;
 *         &lt;element name="DepartmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LongestCurrentWaitTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PatientsStillWaiting" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="TotalPatientsForAverage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepartmentWaitTimes", propOrder = {
    "averageWaitTime",
    "departmentIDs",
    "departmentName",
    "longestCurrentWaitTime",
    "patientsStillWaiting",
    "totalPatientsForAverage"
})
public class DepartmentWaitTimes {

    @XmlElementRef(name = "AverageWaitTime", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<String> averageWaitTime;
    @XmlElementRef(name = "DepartmentIDs", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfIDType> departmentIDs;
    @XmlElementRef(name = "DepartmentName", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<String> departmentName;
    @XmlElementRef(name = "LongestCurrentWaitTime", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<String> longestCurrentWaitTime;
    @XmlElement(name = "PatientsStillWaiting")
    protected Integer patientsStillWaiting;
    @XmlElementRef(name = "TotalPatientsForAverage", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<String> totalPatientsForAverage;

    /**
     * Gets the value of the averageWaitTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAverageWaitTime() {
        return averageWaitTime;
    }

    /**
     * Sets the value of the averageWaitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAverageWaitTime(JAXBElement<String> value) {
        this.averageWaitTime = value;
    }

    /**
     * Gets the value of the departmentIDs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}
     *     
     */
    public JAXBElement<ArrayOfIDType> getDepartmentIDs() {
        return departmentIDs;
    }

    /**
     * Sets the value of the departmentIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}
     *     
     */
    public void setDepartmentIDs(JAXBElement<ArrayOfIDType> value) {
        this.departmentIDs = value;
    }

    /**
     * Gets the value of the departmentName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets the value of the departmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDepartmentName(JAXBElement<String> value) {
        this.departmentName = value;
    }

    /**
     * Gets the value of the longestCurrentWaitTime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLongestCurrentWaitTime() {
        return longestCurrentWaitTime;
    }

    /**
     * Sets the value of the longestCurrentWaitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLongestCurrentWaitTime(JAXBElement<String> value) {
        this.longestCurrentWaitTime = value;
    }

    /**
     * Gets the value of the patientsStillWaiting property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPatientsStillWaiting() {
        return patientsStillWaiting;
    }

    /**
     * Sets the value of the patientsStillWaiting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPatientsStillWaiting(Integer value) {
        this.patientsStillWaiting = value;
    }

    /**
     * Gets the value of the totalPatientsForAverage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTotalPatientsForAverage() {
        return totalPatientsForAverage;
    }

    /**
     * Sets the value of the totalPatientsForAverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTotalPatientsForAverage(JAXBElement<String> value) {
        this.totalPatientsForAverage = value;
    }

}
