
package com.chi.wsclient.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MinutesToLookBack" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="UseAppointmentTimeIfAfterStartEvent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="CalculateForOverallLevel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="CalculateForEachDepartment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Departments" type="{urn:Epic-com:Scheduling.2012.Services.Patient}ArrayOfIDType" minOccurs="0"/&gt;
 *         &lt;element name="StartEvent" type="{urn:Epic-com:Scheduling.2012.Services.Patient}IDType" minOccurs="0"/&gt;
 *         &lt;element name="EndEvent" type="{urn:Epic-com:Scheduling.2012.Services.Patient}IDType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "minutesToLookBack",
    "useAppointmentTimeIfAfterStartEvent",
    "calculateForOverallLevel",
    "calculateForEachDepartment",
    "departments",
    "startEvent",
    "endEvent"
})
@XmlRootElement(name = "GetDepartmentWaitTimes")
public class GetDepartmentWaitTimes {

    @XmlElementRef(name = "MinutesToLookBack", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> minutesToLookBack;
    @XmlElementRef(name = "UseAppointmentTimeIfAfterStartEvent", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> useAppointmentTimeIfAfterStartEvent;
    @XmlElementRef(name = "CalculateForOverallLevel", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> calculateForOverallLevel;
    @XmlElementRef(name = "CalculateForEachDepartment", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> calculateForEachDepartment;
    @XmlElementRef(name = "Departments", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfIDType> departments;
    @XmlElementRef(name = "StartEvent", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<IDType> startEvent;
    @XmlElementRef(name = "EndEvent", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<IDType> endEvent;

    /**
     * Gets the value of the minutesToLookBack property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getMinutesToLookBack() {
        return minutesToLookBack;
    }

    /**
     * Sets the value of the minutesToLookBack property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setMinutesToLookBack(JAXBElement<Integer> value) {
        this.minutesToLookBack = value;
    }

    /**
     * Gets the value of the useAppointmentTimeIfAfterStartEvent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUseAppointmentTimeIfAfterStartEvent() {
        return useAppointmentTimeIfAfterStartEvent;
    }

    /**
     * Sets the value of the useAppointmentTimeIfAfterStartEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUseAppointmentTimeIfAfterStartEvent(JAXBElement<Boolean> value) {
        this.useAppointmentTimeIfAfterStartEvent = value;
    }

    /**
     * Gets the value of the calculateForOverallLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getCalculateForOverallLevel() {
        return calculateForOverallLevel;
    }

    /**
     * Sets the value of the calculateForOverallLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setCalculateForOverallLevel(JAXBElement<Boolean> value) {
        this.calculateForOverallLevel = value;
    }

    /**
     * Gets the value of the calculateForEachDepartment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getCalculateForEachDepartment() {
        return calculateForEachDepartment;
    }

    /**
     * Sets the value of the calculateForEachDepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setCalculateForEachDepartment(JAXBElement<Boolean> value) {
        this.calculateForEachDepartment = value;
    }

    /**
     * Gets the value of the departments property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}
     *     
     */
    public JAXBElement<ArrayOfIDType> getDepartments() {
        return departments;
    }

    /**
     * Sets the value of the departments property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfIDType }{@code >}
     *     
     */
    public void setDepartments(JAXBElement<ArrayOfIDType> value) {
        this.departments = value;
    }

    /**
     * Gets the value of the startEvent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IDType }{@code >}
     *     
     */
    public JAXBElement<IDType> getStartEvent() {
        return startEvent;
    }

    /**
     * Sets the value of the startEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IDType }{@code >}
     *     
     */
    public void setStartEvent(JAXBElement<IDType> value) {
        this.startEvent = value;
    }

    /**
     * Gets the value of the endEvent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IDType }{@code >}
     *     
     */
    public JAXBElement<IDType> getEndEvent() {
        return endEvent;
    }

    /**
     * Sets the value of the endEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IDType }{@code >}
     *     
     */
    public void setEndEvent(JAXBElement<IDType> value) {
        this.endEvent = value;
    }

}
