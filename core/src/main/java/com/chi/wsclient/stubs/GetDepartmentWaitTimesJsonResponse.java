
package com.chi.wsclient.stubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetDepartmentWaitTimes_JsonResult" type="{urn:Epic-com:Scheduling.2014.Services.Utility}Utility.GetDepartmentWaitTimesResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDepartmentWaitTimesJsonResult"
})
@XmlRootElement(name = "GetDepartmentWaitTimes_JsonResponse")
public class GetDepartmentWaitTimesJsonResponse {

    @XmlElementRef(name = "GetDepartmentWaitTimes_JsonResult", namespace = "urn:Epic-com:Scheduling.2014.Services.Utility", type = JAXBElement.class, required = false)
    protected JAXBElement<UtilityGetDepartmentWaitTimesResponse> getDepartmentWaitTimesJsonResult;

    /**
     * Gets the value of the getDepartmentWaitTimesJsonResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UtilityGetDepartmentWaitTimesResponse }{@code >}
     *     
     */
    public JAXBElement<UtilityGetDepartmentWaitTimesResponse> getGetDepartmentWaitTimesJsonResult() {
        return getDepartmentWaitTimesJsonResult;
    }

    /**
     * Sets the value of the getDepartmentWaitTimesJsonResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UtilityGetDepartmentWaitTimesResponse }{@code >}
     *     
     */
    public void setGetDepartmentWaitTimesJsonResult(JAXBElement<UtilityGetDepartmentWaitTimesResponse> value) {
        this.getDepartmentWaitTimesJsonResult = value;
    }

}
