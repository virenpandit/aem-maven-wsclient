
package com.chi.wsclient.stubs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfDepartmentWaitTimes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfDepartmentWaitTimes"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DepartmentWaitTimes" type="{urn:Epic-com:Scheduling.2014.Services.Utility}DepartmentWaitTimes" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDepartmentWaitTimes", propOrder = {
    "departmentWaitTimes"
})
public class ArrayOfDepartmentWaitTimes {

    @XmlElement(name = "DepartmentWaitTimes", nillable = true)
    protected List<DepartmentWaitTimes> departmentWaitTimes;

    /**
     * Gets the value of the departmentWaitTimes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the departmentWaitTimes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepartmentWaitTimes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DepartmentWaitTimes }
     * 
     * 
     */
    public List<DepartmentWaitTimes> getDepartmentWaitTimes() {
        if (departmentWaitTimes == null) {
            departmentWaitTimes = new ArrayList<DepartmentWaitTimes>();
        }
        return this.departmentWaitTimes;
    }

}
