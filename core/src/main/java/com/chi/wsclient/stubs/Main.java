package com.chi.wsclient.stubs;


public class Main {

  public static void log(Object o) {
  	System.out.println("" + o);
  }

  public static void main(final String[] args) throws Exception {
    log("Inside main 2.6");
    initTracing();
    final IUtility2014 utility2014 = new Utility().getIUtility2014();

    javax.xml.ws.BindingProvider bp = (javax.xml.ws.BindingProvider) utility2014;
    javax.xml.ws.soap.SOAPBinding binding = (javax.xml.ws.soap.SOAPBinding) bp.getBinding();
    binding.setMTOMEnabled(false);
    
    UtilityGetDepartmentWaitTimesResponse departmentWaitTimes = utility2014.getDepartmentWaitTimes(60, true, true, true, createDepartments(), null, null);
    log("After System.call");
    System.out.println(String.format("%s ==> %s", "LongestCurrentWaitTime", departmentWaitTimes.getLongestCurrentWaitTime()));
  }

  private static void initTracing() {
    System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
    System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
    System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

  }

  private static ArrayOfIDType createDepartments() {
    final ObjectFactory objectFactory = new ObjectFactory();
    final ArrayOfIDType departments = new ArrayOfIDType();
    final IDType department = new IDType();
    department.setID(objectFactory.createIDTypeID("101031100"));
    departments.getIDType().add(department);
    return departments;
  }
}
